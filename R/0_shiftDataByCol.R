shiftDataByCol = function(dat) {
    nr = nrow(dat)
    nc = ncol(dat)
    debugps("Shifting data col by col...")
    debugps("dat has", nr, "rows")
    debugps("dat has", nc, "cols")

    debugps("Head of dat before shifting: ")
    debugpo(head(dat))
    debugps("Tail of dat before shifting: ")
    debugpo(tail(dat))

    debugps("initialize a matrix newdat with same size as dat...")
    newdat = matrix(NA, nr, nc)
    for(i in 1:nc) {
        colShiftN = as.integer(colnames(dat)[i])
        debugps("This col should be shifted by ", colShiftN, "according to colname")
        newdat[1:(nr-colShiftN), i] = dat[(colShiftN+1):nr, 1]
    }

    debugps("Setting colnames for new dat...")
    colnames(newdat) = colnames(dat)
    debugps("Head of dat after shifting: ")
    debugpo(head(newdat))
    debugps("Tail of dat after shifting: ")
    debugpo(tail(newdat))

    newdat
}
