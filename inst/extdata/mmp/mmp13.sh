plink --allow-no-sex --bfile mmp13 \
    --covar mmp13.phe --covar-name Sex,Cage,UnileverBatch,Wrinkles,Batch,Res \
    --pheno mmp13.phe --pheno-name Page \
    --linear hide-covar \
    --out mmp13
